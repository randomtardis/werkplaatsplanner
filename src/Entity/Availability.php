<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AvailabilityRepository")
 * @ORM\Table(name="Availability" , uniqueConstraints={@ORM\UniqueConstraint(name="branch_date_idx", columns={"branch", "work_date"} )})
 *
 */

class Availability
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=70)
     */
    private $branch;

    /**
     * @ORM\Column(type="date")
     */
    private $workDate;

    /**
     * @ORM\Column(type="integer")
     */
    private $hours;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBranch(): ?string
    {
        return $this->branch;
    }

    public function setBranch(string $branch): self
    {
        $this->branch = $branch;

        return $this;
    }

    public function getworkDate(): ?\DateTimeInterface
    {
        return $this->workDate;
    }

    public function setworkDate(\DateTimeInterface $workDate): self
    {
        $this->workDate = $workDate;

        return $this;
    }

    public function getHours(): ?int
    {
        return $this->hours;
    }

    public function setHours(int $hours): self
    {
        $this->hours = $hours;

        return $this;
    }
}
