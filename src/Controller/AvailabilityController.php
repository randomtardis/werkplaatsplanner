<?php

namespace App\Controller;

use App\Entity\Availability;
use App\Form\AvailabilityType;
use App\Form\BatchAvailabilityType;
use App\Repository\AvailabilityRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/availability")
 */
class AvailabilityController extends AbstractController
{
    /**
     * @Route("/", name="availability_index", methods="GET")
     */
    public function index(AvailabilityRepository $availabilityRepository): Response
    {
        return $this->render('availability/index.html.twig', ['availabilities' => $availabilityRepository->findAll()]);
    }

    /**
     * @Route("/new", name="availability_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $availability = new Availability();
        $form = $this->createForm(AvailabilityType::class, $availability);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($availability);
            $em->flush();

            return $this->redirectToRoute('availability_index');
        }

        return $this->render('availability/new.html.twig', [
            'availability' => $availability,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/calenderOmmen", name="availability_calenderOmmen", methods="GET|POST")
     */
    public function calenderOmmen(AvailabilityRepository $availabilityRepository): Response
    {
        return $this->render('availability/calender.html.twig', [
            'availabilities' => $availabilityRepository->findByBranchAndDateRange('Ommen', new DateTime('first day of this month'), new DateTime('last day of this month')),
            'time' => new \DateTime(),
            'location' => 'Ommen',
        ]);
    }

    /**
     * @Route("/calenderHardenberg", name="availability_calenderHardenberg", methods="GET|POST")
     */
    public function calenderHardenberg(AvailabilityRepository $availabilityRepository): Response
    {


        return $this->render('availability/calender.html.twig', [
            'availabilities' => $availabilityRepository->findByBranchAndDateRange('Hardenberg', new DateTime('first day of this month'), new DateTime('last day of this month')),
            'time' => new \DateTimeImmutable(),
            'location' => 'Hardenberg',
        ]);
    }

    /**
     * @Route("/availability_calenderLastMonth", name="availability_calenderLastMonth", methods="GET|POST")
     */
    public function calenderLastMonth(AvailabilityRepository $availabilityRepository, request $request): Response
    {

        $date = new \DateTimeImmutable($request->query->get('date'));
        return $this->render('availability/calender.html.twig', [
            'availabilities' => $availabilityRepository->findByBranchAndDateRange($request->query->get('location'),
                $date,
                $date->modify('last day of this month')),
            'time' => $date,
            'location' => $request->query->get('location'),
        ]);
    }

    /**
     * @Route("/availability_calenderNextMonth", name="availability_calenderNextMonth", methods="GET|POST")
     */

    public function calenderNextMonth(AvailabilityRepository $availabilityRepository, request $request): Response
    {

        $date = new \DateTimeImmutable($request->query->get('date'));
        return $this->render('availability/calender.html.twig', [
            'availabilities' => $availabilityRepository->findByBranchAndDateRange($request->query->get('location'),
                $date,
                $date->modify('last day of this month')),
            'time' => $date,
            'location' => $request->query->get('location'),
        ]);
    }

    /**
     * @Route("/batch", name="availability_batch", methods="GET|POST")
     */
    public function Batch(Request $request): Response
    {
        $form = $this->createForm(BatchAvailabilityType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data    = $form->getData();
            $em = $this->getDoctrine()->getManager();

            $startWorkDate = $data['startWorkDate'];
            $endWorkDate = $data['endWorkDate'];

            $occupied = $this
                ->getDoctrine()
                ->getRepository(Availability::class)
                ->findByBranchAndDateRange($data['branch'], $data['startWorkDate'], $data['endWorkDate'])
            ;

            while ($startWorkDate <= $endWorkDate){
                if(!isset($occupied[$startWorkDate->format('j')]) && \in_array($startWorkDate->format('N'), $data['days'])){

                    $availability = new Availability();
                    $availability->setBranch($data['branch']);
                    $availability->setHours($data['hours']);
                    $availability->setworkDate(clone $startWorkDate);
                    $em->persist($availability);
                }
                $startWorkDate->modify('+1 day');
            }
            $em->flush();

            return $this->redirectToRoute('availability_calender');
        }

        return $this->render('availability/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="availability_show", methods="GET")
     */
    public function show(Availability $availability): Response
    {
        return $this->render('availability/show.html.twig', ['availability' => $availability]);
    }

    /**
     * @Route("/{id}/edit", name="availability_edit", methods="GET|POST")
     */
    public function edit(Request $request, Availability $availability): Response
    {
        $form = $this->createForm(AvailabilityType::class, $availability);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('availability_edit', ['id' => $availability->getId()]);
        }

        return $this->render('availability/edit.html.twig', [
            'availability' => $availability,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="availability_delete", methods="DELETE")
     */
    public function delete(Request $request, Availability $availability): Response
    {
        if ($this->isCsrfTokenValid('delete'.$availability->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($availability);
            $em->flush();
        }

        return $this->redirectToRoute('availability_index');
    }
}
