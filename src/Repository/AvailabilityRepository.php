<?php

namespace App\Repository;

use App\Entity\Availability;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Availability|null find($id, $lockMode = null, $lockVersion = null)
 * @method Availability|null findOneBy(array $criteria, array $orderBy = null)
 * @method Availability[]    findAll()
 * @method Availability[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AvailabilityRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Availability::class);
    }

    public function findByBranchAndDateRange($branch, $startWorkDate, $endWorkDate)
    {
        $result = [];
        /** @var Availability[] $records */
        $records = $this->createQueryBuilder('a')
            ->andWhere('a.branch = :branch')
            ->andWhere('a.workDate BETWEEN :startWorkDate AND :endWorkDate')
            ->orderBy('a.workDate')
            ->setParameters([
                'branch' => $branch,
                'startWorkDate' => $startWorkDate,
                'endWorkDate' => $endWorkDate
            ])
            ->getQuery()
            ->getResult()
            ;

        foreach ($records as $record) {
            $result[$record->getworkDate()->format('j')] = $record;
        }
        dump($result);
        return $result;
    }

//    /**
//     * @return Availability[] Returns an array of Availability objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Availability
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
